package com.stock.mvc.entites;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
@Entity
public class Category implements Serializable{
@Id
@GeneratedValue
private long idCategory;
private String code;
private String designation;
private List<Article> articles;

public Category() {

}

private long getIdCategory() {
	return idCategory;
}

private void setIdCategory(long id) {
	this.idCategory = id;
}

public String getCode() {
	return code;
}

public void setCode(String code) {
	this.code = code;
}

public String getDesignation() {
	return designation;
}

public void setDesignation(String designation) {
	this.designation = designation;
}


@OneToMany(mappedBy="category")
public List<Article> getArticles() {
	return articles;
}

public void setArticles(List<Article> articles) {
	this.articles = articles;
}

}
